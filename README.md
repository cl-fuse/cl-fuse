cl-fuse
=======

It is a mirror of CL-Fuse monotone repository maintained by the original author. 
CL-Fuse is stable enough that difference is minimal, but the true repository is in Monotone.
It was previously published at http://mtn-host.prjek.net/viewmtn/cl-fuse/branch/changes/com.ignorelist.401a0bf1.raskin.cl-fuse.bindings 
but currently this hosting is unfortunately offline